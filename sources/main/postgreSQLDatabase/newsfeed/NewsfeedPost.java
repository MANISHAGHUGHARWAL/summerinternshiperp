/**
 * 
 */
package postgreSQLDatabase.newsfeed;

import java.util.ArrayList;
import java.util.Date;

/**
 * @author Shubhi
 *
 */
public class NewsfeedPost {
private long post_id,post_author;
private int post_privacy;
private ArrayList<Integer> post_comments,post_likes;
private String post_text;
private Date post_time;
private int likes_count,comments_count;
/**
 * @return the likes_count
 */
public int getLikes_count() {
	return likes_count;
}
/**
 * @param likes_count the likes_count to set
 */
public void setLikes_count(int likes_count) {
	this.likes_count = likes_count;
}
/**
 * @return the comments_count
 */
public int getComments_count() {
	return comments_count;
}
/**
 * @param comments_count the comments_count to set
 */
public void setComments_count(int comments_count) {
	this.comments_count = comments_count;
}
/**
 * @return the post_id
 */
public long getPost_id() {
	return post_id;
}
/**
 * @param post_id the post_id to set
 */
public void setPost_id(long post_id) {
	this.post_id = post_id;
}
/**
 * @return the post_author
 */
public long getPost_author() {
	return post_author;
}
/**
 * @param post_author the post_author to set
 */
public void setPost_author(long post_author) {
	this.post_author = post_author;
}
/**
 * @return the post_privacy
 */

/**
 * @return the privacy
 */

/**
 * @param post_privacy2 the privacy to set
 */
public String getPost_privacy() {
	int post_privacy=this.post_privacy;
	if(post_privacy==0){
		return "Shared Publicly";
	}
	else if(post_privacy==1){
		return "Friends";
	}
	else if(post_privacy==2){
		return "Only Me";
	}
	return null;
}
/**
 * @param post_privacy the post_privacy to set
 */
public void setPost_privacy(int post_privacy) {
	this.post_privacy=post_privacy;
	
}
/**
 * @return the post_comments
 */
public ArrayList<Integer> getPost_comments() {
	return post_comments;
}
/**
 * @param post_comments the post_comments to set
 */
public void setPost_comments(ArrayList<Integer> post_comments) {
	this.post_comments = post_comments;
}
/**
 * @return the post_likes
 */
public ArrayList<Integer> getPost_likes() {
	return post_likes;
}
/**
 * @param post_likes the post_likes to set
 */
public void setPost_likes(ArrayList<Integer> post_likes) {
	this.post_likes = post_likes;
}
/**
 * @return the post_text
 */
public String getPost_text() {
	return post_text;
}
/**
 * @param post_text the post_text to set
 */
public void setPost_text(String post_text) {
	this.post_text = post_text;
}
/**
 * @return the post_time
 */
public Date getPost_time() {
	return post_time;
}
/**
 * @param post_time the post_time to set
 */
public void setPost_time(Date post_time) {
	this.post_time = post_time;
}
}
