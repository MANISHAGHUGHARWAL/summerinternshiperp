package actions.registration;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import exceptions.IncorrectFormatException;
import postgreSQLDatabase.registration.Query;
import users.Student;

/**
 * Servlet implementation class VerifyStudent
 */

@WebServlet(name = "Verify Student Servlet", urlPatterns = { "/VerifyStudent" })
public class VerifyStudent extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public VerifyStudent() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.sendError(500);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		Long reg_id = Long.parseLong(request.getParameter("reg_id"));
		Query.updateVerified(reg_id);
		Student student = null;
		try {
			student = Query.getRegistrationStudentData(reg_id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IncorrectFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Date date = new Date();
		Integer year = Integer.parseInt(new SimpleDateFormat("yyyy").format(date));
		postgreSQLDatabase.feePayment.Query.addFeeTransaction(1, student.getCategory(), reg_id, year);
		PrintWriter pw = response.getWriter();
		pw.write("updated");
	}

}
