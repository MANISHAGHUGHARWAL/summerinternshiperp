package actions.feePayment;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;

import exceptions.IncorrectFormatException;
import postgreSQLDatabase.feePayment.Query;

/**
 * Servlet implementation class AddPaymentInfo
 */

@WebServlet(name = "Add paymet Info Servlet", urlPatterns = { "/AddPaymentInfo" })
public class AddPaymentInfo extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AddPaymentInfo() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.sendError(500);
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// challan 5
		HttpSession session = request.getSession();
		int sem = Integer.parseInt(request.getParameter("semester"));
		long reg_id = Long.parseLong(session.getAttribute("reg_id").toString());
		long transaction_id = 0;
		try {
			transaction_id=postgreSQLDatabase.feePayment.Query.getTransactionId(Integer.parseInt(new SimpleDateFormat("yyyy").format(new Date())), sem, reg_id);
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println(request.getParameter("semester"));
		if (request.getParameter("payment_method").toString().equals("demand_draft")) {
		
			JSONObject details = new JSONObject();
			details.put("amount", request.getParameter("amount"));
			details.put("bank", request.getParameter("bank"));
			details.put("date", request.getParameter("date"));
			details.put("dd_no", request.getParameter("dd_no"));
			Long ref_id = 0L;
		
				
				ref_id = Query.addFeePayment("DD payment", 5, details, Long.parseLong(request.getParameter("amount")),
						reg_id,reg_id,transaction_id);
				postgreSQLDatabase.registration.Query.updateVerificationStatus(3, reg_id);
		
			PrintWriter writer = response.getWriter();
			JSONObject ref = new JSONObject();
			ref.put("ref_id", ref_id);
			writer.write(ref.toString());

		}

		if (request.getParameter("payment_method").equals("neft")) {
		
			JSONObject details = new JSONObject();
			details.put("amount", request.getParameter("amount"));
			details.put("bank", request.getParameter("bank"));
			details.put("date", request.getParameter("date"));
			details.put("tid", request.getParameter("tid"));
			Long ref_id = 0L;
			
				ref_id = Query.addFeePayment("NEFT payment", 4, details, Long.parseLong(request.getParameter("amount")),
						reg_id,	reg_id,transaction_id);
			
			PrintWriter writer = response.getWriter();
			JSONObject ref = new JSONObject();
			ref.put("ref_id", ref_id);
			writer.write(ref.toString());
		}
		if (request.getParameter("payment_method").equals("challan")) {
		
			JSONObject details = new JSONObject();
			details.put("amount", request.getParameter("amount"));
			details.put("bank", request.getParameter("bank"));
			details.put("date", request.getParameter("date"));
			details.put("challan_no", request.getParameter("challan_no"));
		Long ref_id = 0L;
			
				ref_id = Query.addFeePayment("Challan payment", 2, details,
						Long.parseLong(request.getParameter("amount")),
						reg_id, reg_id,transaction_id);
			
			PrintWriter writer = response.getWriter();
			JSONObject ref = new JSONObject();
			ref.put("ref_id", ref_id);
			writer.write(ref.toString());
		}
	}

}
