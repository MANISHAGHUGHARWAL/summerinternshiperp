package actions.chats;

import java.io.IOException;


import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import settings.database.PostgreSQLConnection;

@WebServlet(
		name="New message Servlet",
		urlPatterns={"/NewMessage"}
	)
/**
 * Servlet implementation class NewMessage
 * @author Megha
 * Servlet to add new message to database.
 *  
 */
public class NewMessage extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public NewMessage() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.sendError(500);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 * Parameters message-text, author id, conversation id
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO remove hard coding
		try {
			PostgreSQLConnection.getConnection().prepareStatement("SELECT public.\"newMessage\"(?,?,?);");
			String text=request.getParameter("text");
			long erpId=Long.parseLong(request.getSession().getAttribute("erpId").toString());
			long conversation_id=Long.parseLong(request.getParameter("conversation_id"));
			postgreSQLDatabase.chats.Query.newMessage(text,erpId,conversation_id);
			//System.out.println(proc.toString());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}






	}

}
