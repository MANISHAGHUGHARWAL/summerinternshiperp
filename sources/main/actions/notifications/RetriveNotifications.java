package actions.notifications;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import postgreSQLDatabase.notifications.Notifications;
import settings.database.PostgreSQLConnection;

/**
 * Servlet implementation class RetriveNotifications
 */

@WebServlet(
		name="Retrieve Notifications Servlet",
		urlPatterns={"/RetriveNotifications"}
	)
public class RetriveNotifications extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public RetriveNotifications() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.sendError(500);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter writer = response.getWriter();
		PreparedStatement proc;
		HttpSession session = request.getSession();
		ArrayList<Notifications> notifications = postgreSQLDatabase.notifications.Query.getUnreadNotifications(Long.parseLong(session.getAttribute("erpId").toString()));
			Iterator<Notifications> iterator = notifications.iterator();

			JSONArray notifications_array = new JSONArray();
			JSONObject notifications_object;
			while (iterator.hasNext()) {
				notifications_object = new JSONObject();
				Notifications current = iterator.next();
				notifications_object.put("notif_id", current.getId());
				notifications_object.put("type", current.getType());
				notifications_object.put("message", current.getMessage());
				notifications_object.put("link", current.getLink());
				notifications_object.put("timestamp",
						new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(current.getTimestamp()));
				notifications_object.put("expiry",
						new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSSSSS").format(current.getExpiry()));
				notifications_array.put(notifications_object);
			}

			
			writer.write(notifications_array.toString());
			
	}

	}
