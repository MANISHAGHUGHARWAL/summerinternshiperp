package actions.authentication;
import java.util.Iterator;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import postgreSQLDatabase.authentication.AutoSuggestUsers;



/**
 * @author Joey
 * This class is meant to generate name suggestions for text on type
 * @category AJAX quick response servlet
 *
 */
@WebServlet(
	name="Auto Suggest Servlet",
	urlPatterns={"/AutoSuggest"}
)
public class AutoSuggest extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
	private final static Logger LOGGER = Logger.getLogger(AutoSuggest.class.getName());
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Add redirect to error page
		PrintWriter writer = response.getWriter();
		String word=request.getParameter("string");
		String domain="";
		if(request.getParameter("domain")==null);
		else if(request.getParameter("domain").equals("students")){
		domain=request.getParameter("domain");
		}
		else{		}
		ArrayList<AutoSuggestUsers> a_list=postgreSQLDatabase.authentication.Query.getAutoSuggest(word,domain);
		JSONObject query_object=new JSONObject();
		query_object.put("total_count",a_list.size());
		query_object.put("incomplete_results", false);
		
		
		
		
		JSONArray jarray=new JSONArray();
		Iterator<AutoSuggestUsers> iterator = a_list.iterator();
		while(iterator.hasNext()){
			
			JSONObject j_object= new JSONObject();
			AutoSuggestUsers current=iterator.next();
			j_object.put("email",current.getEmail());
			j_object.put("full_name",current.getName());
			j_object.put("user_type",current.getUser_type());
			j_object.put("id",current.getUsername());
			if(current.getStudent_id()!=null)j_object.put("student_id",current.getStudent_id());
			jarray.put(j_object);
			}
		query_object.put("items",jarray );
		
		writer.write(query_object.toString());
	
	}

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendError(500);
//		writer.write(postgreSQLDatabase.authentication.Query.getAutoSuggest(request.getParameter("string")).toString());
		
	}

}
