package actions.authentication;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class VerifyForgotPassword
 */
@WebServlet("/VerifyForgotPassword")
public class VerifyForgotPassword extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public VerifyForgotPassword() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendError(500);
		String username=request.getParameter("username");
		String vercode=request.getParameter("vercode");
		Long erp_id=postgreSQLDatabase.authentication.Query.VerifyForgotPassword(username, vercode);
		System.out.println(erp_id);
		if(erp_id!=0l){
			request.getSession().setAttribute("forgot_erp_id",erp_id);
			request.getSession().setAttribute("forgot_username",username);
			response.sendRedirect("forgotReset.jsp");
		}
		else{
			response.sendError(500);
			
		}
		


	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
